# molyjam

"The Shadowland Prophesy", a game I made in seven hours for Molyjam 2012. This is just the source; please see the wiki page.

**This repository has [moved](https://github.com/mcclure/bitbucket-backup/tree/archive/repos/molyjam).**
